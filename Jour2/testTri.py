import unittest
import app

class TestSort(unittest.TestCase):

    def test_sort(self):
        self.assertEqual(app.sort([12, 35, 84]), [12, 35, 84])
        self.assertEqual(app.sort([12, 84, 35]), [12, 35, 84])

    def test_duplicate(self):
        self.assertEqual(app.sort([12, 84, 35, 84]), [12, 35, 84, 84])

    def test_negative(self):
        self.assertEqual(app.sort([35, 12, -84]), [-84, 12, 35])

    def test_empty(self):
        self.assertEqual(app.sort([]), [])

if __name__ == '__main__':

    unittest.main()