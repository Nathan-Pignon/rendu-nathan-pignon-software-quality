def sentenceVerification(sentence):
    if sentence[0][0].islower():
        return False
    elif len(sentence.split()) <= 2 or len(sentence.split()) >= 10:
        return False
    elif sentence[-1] != ".":
        return False
    else:
        return True

def main():
    print(sentenceVerification(str(input("Votre phrase : "))))

if __name__ == "__main__":
    main()