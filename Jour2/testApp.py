import unittest
import app

class TestFonc(unittest.TestCase):

    def test_parse_value_space(self):
        self.assertListEqual(app.parse_value('12 35 84'), [12,35,84])

    def test_parse_value_spaces(self):
        self.assertListEqual(app.parse_value('12 35        84'), [12,35,84])

    def test_parse_value_return(self):
        self.assertListEqual(app.parse_value('123\n584'), [123,584])

    def test_parse_value_negative(self):
        self.assertListEqual(app.parse_value('123 -584'), [123,-584])

    def test_parse_value_symbol(self):
        self.assertListEqual(app.parse_value('123 --584'), [123])

    def test_parse_value_text(self):
        self.assertListEqual(app.parse_value('123584 toto'), [123584])

    def test_parse_value_void_list(self):
        self.assertListEqual(app.parse_value('toto --'), [])

if __name__ == '__main__':

    unittest.main()