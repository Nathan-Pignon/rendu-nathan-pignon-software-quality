# Groupe Cowork 5 : Eliott Clavier, Nathan Pignon, Paul Ripault, Maxime Martin
def read_file(file):
    f = open(file)
    return f.read()


def parse_value(string):
    array = string.split()
    merge_array = []

    for el in array:
        try:
            merge_array.append(int(el))
        except ValueError:
            continue
    return merge_array


def sort(array, first_index=None, last_index=None):
    if not first_index and not last_index:
        first_index, last_index = 0, len(array) - 1

    if first_index < last_index:
        # On trie le tableau en fonction d'une valeur pivot choisi, et on retourne l'index du pivot une fois les valeurs replacées dans
        # la partition de ce tour de boucle
        pivot = partition(array, first_index, last_index)
        # Sort de la partie à gauche du pivot
        sort(array, first_index, pivot - 1)
        # Sort de la partie à droite du pivot
        sort(array, pivot + 1, last_index)
    return array


def partition(array, first_index, last_index):
    # On prend comme valeur pivot la première valeur du segment de tableau à partionner
    value_pivot = array[first_index]

    # Donc l'index le plus bas à évaluer est le premier du tableau après la valeur pivot
    lower = first_index + 1

    # L'index le plus haut est le dernier du tableau
    upper = last_index

    done = False

    # Tant que le sous tableau n'est pas trié
    while not done:

        # Tant que les valeurs à gauche sont inférieures au pivot
        while lower <= upper and array[lower] <= value_pivot:
            lower += 1

        # Tant que les valeurs à droite sont supérieures au pivot
        while array[upper] >= value_pivot and upper >= lower:
            upper -= 1

        # Si les deux index plus petit et plus grand se croisent, alors la partition est triée (tous les nombres inférieurs au pivot
        # à gauche de la partition, tous les nombres supérieurs au pivot à droite de la partition)
        if upper < lower:
            done = True

        # Sinon, cela veut dire que le nombre à l'index lower est plus grand que le pivot et que le nombre à l'index upper
        # est plus petit que le pivot, donc on échange les deux valeurs pour les placer du bon côté et on reprend la boucle while
        else:
            array[lower], array[upper] = array[upper], array[lower]


    # Echange entre le pivot et la valeur la plus à gauche
    array[first_index], array[upper] = array[upper], array[first_index]
    return upper


if __name__ == '__main__':
    print(sort([12, 86, 78, 45,76,1,2,5,6454,422222,478,77,48545]))
